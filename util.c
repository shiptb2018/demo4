#define _BSD_SOURCE  // needed if c99 compiler option is used

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "daq_util.h"
 
uint32_t PoissnT(int av_us) {
  /* 
   * Poissonian random time interval generator 
   * av_us - average time in us
   * Author: public domain
   */
  return (uint32_t) (round(-log(1.0l - (double) random() / RAND_MAX)* av_us)) ;
}

int PoissnN(double av) {
  /* 
   * Poissonian random number generator
   * av - expected average
   * Author: PG + public domain, Jan 2018
   */
  if(av <25.) {
    // for av<25, use the brute force algorithm
    int n = 0; //counter of iteration
    double limit; 
    double x;  //pseudo random number
    limit = exp(-av);
    x = (double)random() / RAND_MAX; 
    while (x > limit) {
      n++;
      x *= (double)random() / RAND_MAX;
    }
    return n;
  } else {
    // for av>=25, approximated by a normal distribution with the average "av" and rms "sqrt(av)"
    double x = (double)random() / RAND_MAX,
      y = (double)random() / RAND_MAX,
      z = sqrt(av)*(sqrt(-2 * log(x)) * cos(2 * M_PI * y)) + av +0.5;
    return z<=0? 0: (int)floor(z);
  }
}

double gauss(float av) {
    /* 
     * Normal (Gaussian) random number with av=0 and var=1. 
     * Author: public domain
     */
    double x = (double)random() / RAND_MAX,
      y = (double)random() / RAND_MAX,
      z = sqrt(-2 * log(x)) * cos(2 * M_PI * y);
    return z;
  }


double rndm(void) {
  return (double) random() / RAND_MAX;
}


uint32_t getCycleId(int mode) {
  /*
   * Cycle ID generator, according to MJ+PG "Raw data format" memo
   * mode: =0, use OS clock; =1, (not yet implemented) use external id from the central DAQ message 
   * Author: PG, Jan 2018
   */

  static time_t time_offset=-1, time_now;
  if((long int) time_offset==-1) {
    char s[200];
    struct tm tp={0,0,12,8,3,115,0,0,0}; // 8th of April 2015, 12:00 am
    strftime(s,199,"Time offset date: %x time %X",&tp);
    //printf("%s\n",s);
    time_offset=mktime(&tp);
  }
  time_now=time(NULL); 
  //printf("current time=%ld  time0=%ld diff %5.1f years\n",time_now, time_offset,
  //	 difftime(time_now,time_offset)/3.154E7);
  return difftime(time_now,time_offset)*5;
}



/*
 *----------------------------------------------- cmd_flds
 */

#define MAX_ARGS 20
/*
 * To extract up to 20 first words from the text string,
 * separated by (ASCII) blanks, commas
 */
  int cmd_flds (char *line, char ***argv) {
  char *token;
  int k=0;

  if(*argv==NULL) 
    *argv = (char **)malloc(sizeof(char *)*MAX_ARGS); // up to 20 arguments
    
  for(token = strtok(line, " ,\t"),k=0; 
      (token!=NULL) && (k<MAX_ARGS); 
      token = strtok(NULL, " ,\t"), k++) {

      *(*argv+k)=token;
      // printf("--- %d token=|%s|\n",k, *(*argv+k)); fflush(stdout);

    }  
  return k;
  }	 
/*
unsigned char checksum (unsigned char *ptr, size_t sz) {
    unsigned char chk = 0;
    while (sz-- != 0)
        chk -= *ptr++;
    return chk;
}
*/
unsigned char checksum (unsigned char *ptr, size_t sz) {
    unsigned char chk = 0;
    while (sz-- != 0) {
      //printf(" sz %d *ptr %2X chk=%2X \n", (int)sz, *ptr, chk);
      chk -= *ptr++;
    }
    return chk;
}

#define HNAME_LEN 20
char *localHost (void) {
  /* 
   * returns local host's short name
   */
  static char *dot=NULL, hname[HNAME_LEN];
  //  if(dot == NULL) {
    gethostname(hname, HNAME_LEN);
    dot=strchr(hname,'.');
    if(dot == NULL) dot=hname+HNAME_LEN-1;
    *dot = '\0';
    //  }
  return hname;
}


