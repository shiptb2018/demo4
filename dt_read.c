#define _BSD_SOURCE  // needed if c99 compiler option is used

#ifdef DT_READ
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#define BLEN DT_READ

/*
 * Routine to continuously read the most recent file 
 * in RAWDATA_PATH which can be simultaneously updated by another process
 * At a program start, the file is positiond at EOF, in order to skip the
 * old data
 */

char *dt_read(char *dataDir) {
  static char buf[BLEN];
  static char fname[200];
  static FILE *filePtr, *ls; 
  static int ev;
  int nb;
  uint16_t evlen;
  static short *frlen= (short *)buf;
  static long pos0; // current file position

  /* initialization: find and open the data file to watch */
  if(dataDir != NULL) {
    char *c;
    /* shell command to show the most recent file */
    sprintf(buf,"ls -t %s/*.rawdata| head -1", dataDir); 
    //printf("opening pipe to |%s|  dataDir=|%s|\n",buf,dataDir);
    sleep(1); // wait till dtdaq opens output file
    ls = popen(buf,"r");   // pipe to `ls -t $RAWDATA_PATH | head -1` output
    if(ls == NULL) {
      fprintf(stderr," cannot open pipe. check ls -t | head -1\n");
      return NULL;
    }
      
    c = fgets(buf,sizeof(buf),ls);
    pclose(ls);
    if(c != NULL) {
      char *trim=strpbrk(buf,"\n\r"); // trim file name (might contain CR or LF)
      if(trim!=NULL) *trim='\0';
      sprintf(fname,"%s",buf );
      printf ("opening data file |%s|\n", fname);
      filePtr = fopen(fname, "r+");
      assert(filePtr != NULL);
    }
    else {
      fprintf(stderr," cannot find data file. check ls -t | head -1\n");
      return NULL;
    }

    pos0=0L;
    ev=0;

    while (1) {  // skip to EOF
       nb=fread(frlen,sizeof(uint16_t),1,filePtr);
       if(nb !=1) break;
       evlen=*frlen-2;
       fread(buf+2,sizeof(char),evlen,filePtr);
       pos0 = pos0 + *frlen;
       ev++;
     }
    printf("Skipped %d events in %s\n", ev, fname);
    ev = 0;
    return fname;
  }

  /* dataDir==0:  regular entry to read the opened file) */

  fseek(filePtr,pos0,SEEK_SET);
  nb=fread(frlen,sizeof(uint16_t),1,filePtr);
  if(nb !=1) {
     printf("eof after %d ev nb=%d pos0=%ld", ev, nb,pos0); 
     *frlen=-1;  // signal EOF to callin program
  } else {
    ev++;
    if(*frlen >BLEN) *frlen=BLEN;  // limit evsize to buf size
    evlen=*frlen-2;
    //printf("ev=%d evlen=%d nb=%d pos0=%ld\n",ev,evlen,nb,pos0);
    fread(buf+2,sizeof(char),evlen,filePtr);
    pos0 = pos0 + *frlen;
  }
  return buf;
}
#endif
