project=demo4

# Make demo4 for local DAQ stress rests
#   By default, it is compiled with _dynamic_ buffers
#   to compile with static buffers: make buf=BUF_SIZE=xx  (xx: bufsize in MB),
#   or edit the demo4.c header, or edit the makefile
#
#   By default, the command line parameters are interpreted, otherwise
#   make par=NOCMDLINE (or edit the makefile) 
#
#   to re-compile demo4.c, delete demo4.o or use -W... option:
#	make -Wdemo4.c    
#   to recompile everything: make -Wmakefile
#  
#   to add extra compiler options to default -Wall -std=c99:
#	make EXTRA_CFLAG=...  , e.g. make EXTRA_CFLAG=-g (for valgrind)
#
#   to use a preferred compiler add CC={compiler} argument, e.g. 
#   make -Wmakefile CC=g++
#
#   to add the "DT_READ" option (see ./doc/DT_READ.option), add DT_READ=20000
#   argument, where 20000 is the maximal DT event size, in bytes
#
# Important: 
#   The BASE_CH shell variable must be defined as the path to the
#   directory containing lib/ and include/ subdirectorues with
#   ControlHost libraries (libconthost.a and/or libconthost.so) and dispatch.h,
#   e.g.
#   export BASE_CH=/home/user/ControlHost (or /usr/local, if CH is installed)
#
# PG v.1.2 07 June 2018
#-------------------------------------------------------

# buffer allocation (for static, define the buffer size in MB)
buf=DYNAMIC
#buf=BUF_SIZE=30

# interpret command line parameters (otherwise NOCMDLINE)
par=CMDLINE
#par=NOCMDLINE

#----------------------------------------------------- check shell variables
define wrn_
$(info >> Warning: undefined shell variable $(1) (used in $(project) $(2))<<)
endef 
define show_
$(info OK: $(1)=$($(1)))
endef

ifndef BASE_CH
$(info Error: shell variable BASE_CH is not defined)
$(info Read the makefile header info for details)
$(error )
else
$(call show_,BASE_CH)
ifndef CONTROL_HOST
$(call wrn_,CONTROL_HOST,as default dispatcher host)
else 
$(call show_,CONTROL_HOST)
endif
ifndef RAWDATA_PATH
$(call wrn_,RAWDATA_PATH,if recording is enabled)
else 
$(call show_,RAWDATA_PATH)
endif
endif
#----------------------

BASE := .
#BASE_CH = $(CH)/..  # $CH is the directory where ControlHost is deplyed

obj = demo4.o util.o daq.o buffer.o my_detector.o

CFLAGS := $(EXTRA_CFLAG) -Wall -std=c99 -D$(buf) -D$(par) -I$(BASE)/include  -I$(BASE_CH)/include 

ifdef DT_READ
obj := $(obj) dt_read.o
CFLAGS := $(CFLAGS) -DDT_READ=$(DT_READ) 
endif

LDLIBS =  -lpthread -L. -L$(BASE_CH)/lib -lconthost -l$(project) -lm

L = lib$(project).a

$(project) : $(obj) $(L) 
	gcc -o $(project) $(project).o $(LDLIBS)

$(obj) : $(BASE)/include/* makefile 

clean :
	rm -f *.o *~ ./include/*~ $(project) $(project).tgz
tar   :
	rm -f $(project).tgz
	tar -cvzf $(project).tgz *.c include/*.h makefile 
$(L) : $(obj)
	ar vr $@ $?
	ranlib  $@

liblist: 
	nm -s $(BASE)/lib/$(L)

