#ifndef DAQ_H
#define DAQ_H
#include <stdint.h>
#include <stdbool.h>
#include "dispatch.h" // for TAGSIZE
// Macros 
#define NERRS  2      // number of known errors
#define E_EV_OVFL  1  // error "1": event skipped: buffer overflow
#define E_BUF_OVFL 2  // error "2": spill truncated: buf overflow

#define SOR_FLAG 0xFF005C01
#define EOR_FLAG 0xFF005C02
#define SOS_FLAG 0xFF005C03
#define EOS_FLAG 0xFF005C04
#define DUMMY_FRTIME  0xDDDDDDDD;
#define DUMMY_TEXTENT 0xEEEE;

#define DAQCMD "DAQCMD"  // tag reserved for run control commands 

typedef struct {
  uint16_t size;
  uint16_t partitionID;
  uint32_t cycleIdentifier;
  uint32_t frameTime;
  uint16_t timeExtent;
  uint16_t flags; 
} DataFrameHeader;

typedef struct {
  uint16_t channelId;
  uint16_t hitTime;
#ifdef EXTRADATA
  uint16_t extraData;
#endif
} RawDataHit;

typedef struct { 
  DataFrameHeader header;   
  RawDataHit hits[]; 
} DataFrame;

typedef enum {
  SOR=0, EOR, SOS, EOS, ENABLE, DISABLE, STOP,
  WRONG_CMD=-1
} DaqCommand_t;

#define CHNAMELEN 100
typedef struct {
  int RunNumber;
  uint32_t CycleID;
  char CHost[CHNAMELEN];
  char RawTag[TAGSIZE+1];
  char *RecDir;
  int Dbg;
  bool inspill;
  struct {
    bool chksum;
    bool recording;
  } options;
} GlobPars_t; 

// Global variables

extern const char *DaqCmds[];  // defined in daq.c
extern GlobPars_t G;           // defined in daq,c

// daq & run control functions
DaqCommand_t daq_cmd(char *command);
char *daq_mkdir(int run, char *part, int *rc);
int  daq_chmod(const char *path, int mode);


#endif
 
 
