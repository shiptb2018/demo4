#ifndef BUFFER_H
#define BUFFER_H

typedef struct {
  uint32_t BufSizeAlloc;  // set 0 for static buffer
  char *spbuf;            // for static buffer, set to the array address
  char *nextfree;
  size_t spbuf_size;      // for static buffer, set to the array size
  uint32_t ev;            // events/spill  counter 
} Buffer_t;


void *
     buf_getAddr(Buffer_t *buf, int evsize, char *name); // buffer.c
int  buf_reset  (Buffer_t *buf, char *name); // buffer.c
void buf_inhibit(Buffer_t *buf);
#endif
