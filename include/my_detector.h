#ifndef MYDETECTOR_H
#define MYDETECTOR_H
#include "buffer.h"

#define NERRS 2
typedef struct {
  char     name[20];
  uint16_t partId;
  bool     enabled;
  Buffer_t buf;
  unsigned int lasterr;
  unsigned int errs[NERRS];      // error counters
  unsigned int max_buffer;       // largest spill buffer size;
  unsigned int max_hits;         // max no. of hits
  struct {
    int AvNhits; // av. number of hits per ev
    int dly;     // us to wait between put_fullevent
  } sim;
} Detector_t;



DataFrame *my_det_getNextEv    (Detector_t *det) ;
DataFrame *my_dataFrame        (Detector_t *det,uint32_t fType, const void *info, int nbytes ) ;

void      my_det_err           (Detector_t *det, int err);
void      my_det_summary       (Detector_t *det) ;
void      my_det_dumpCycleData (Detector_t *det);
int       my_det_reset         (Detector_t *det) ;
void      my_det_resetClock    (Detector_t *det) ;

#ifdef DT_READ
char      *dt_read(char *dataDir);
#endif

#endif
