#ifndef DAQ_UTIL_H
#define DAQ_UTIL_H
#include <math.h>
#include <stdint.h>

uint32_t getCycleId(int mode);                      // generate valid CycleID
uint32_t PoissnT(int av_us);                        // random time interval (Poissonian)
int      PoissnN(double av);	                    // random whole number (Poissonian)
double   gauss(float av) ;	                    // random Gaussian number
double   rndm(void);                                // uniform random (0.,1.)
int      cmd_flds(char *line, char ***argv);        // get words from a text string
unsigned char 
checksum (unsigned char *ptr, size_t sz);           // compute a byte check sum
char     *localHost (void);                         // local host base name

#endif 

