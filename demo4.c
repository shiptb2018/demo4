#define _BSD_SOURCE  // needed if c99 compiler option is used

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <assert.h>
#include <time.h>
#include <libgen.h> // for basename() 

#include "daq.h"
#include "my_detector.h"
#include "buffer.h"
#include "dispatch.h"
#include "daq_util.h"

/*
 * Buffer_t object is only needed if you use the buffer library (buffer.h)
 * This library offers some convenience, especially for dynamic buffers, 
 * but you are, of course, free to use your own buffer management code.
 */
#define M_version "v.2.1.07.06.2018"
#if defined DYNAMIC
/* initial buffer allobation in MB; re-define, if needed */
#define DBUF_SIZE 10 // 10 MB is good for most of cases
Buffer_t B={
#ifndef __cplusplus
  .BufSizeAlloc=  DBUF_SIZE*1000*1000, // initial buffer allocation
  .spbuf=         NULL,  
  .spbuf_size=    0};
#else
//  ISO C++ does not allow designated initializers 
  DBUF_SIZE*1000*1000, // initial buffer allocation
  NULL, 
  NULL, 
  0,
  0};
#endif
#else

//#define BUF_SIZE   xx   // static buffer size in MB. Define and uncomment this line
char StaticBuf[BUF_SIZE*1000*1000];
Buffer_t B={
  .BufSizeAlloc=  0,    // should be 0 for static buffers
  .spbuf=         StaticBuf,  
  .spbuf_size=    sizeof(StaticBuf) };
#endif


#ifdef DT_READ
  FILE *dtdaq=NULL;
#endif

Detector_t * Det; // the pointer to the detector object created in daq_init

// global variables, parameters and options
extern GlobPars_t G; // declared in daq.h
char *Self; // the program name


void my_daq_close         (int exitcode);
int  my_daq_init          (Detector_t *det, int args, char**argv);
int  my_daq_reactions     (Detector_t *det, 
		          DaqCommand_t cmd, int cmd_args, char **cmd_argv);


void signal_handler       (int signum);

bool inspill(void) ;
void wait_trigger(void);
uint32_t latchTimestamp() ;
void waitSoS (void) ;


/*-------------------------------------------------------------------------- main */
int main(int args, char**argv) {

  int 
    loop,     // command loop number
    rc,       // return code
    i,        // generic int var
    cmd_args; // number of DAQCMD arguments
  char **cmd_argv=NULL;  // command parameters
  char cbuf [200];  // command buffer (long-lived)
  char line [200];  // general-purpose buffer for sprintf (short-lived)
  
  /* intercept Ctrl-C and abort() to ensure a clean program termination */
  signal(SIGINT,  signal_handler);
  signal(SIGABRT, signal_handler);
  setbuf(stdout,NULL); // no buffering in stdout

  // G.Dbg=1;  // the debug level (1 is the default value, see daq.c)

  Self = argv[0];

  /* print program header */
  if(G.Dbg>0) {
#ifndef DYNAMIC
    printf("\n%s with static buf size: %d MB\n", M_version,BUF_SIZE);
#else
    printf("\n%s with dynamic buf, initial size: %d MB\n", M_version,DBUF_SIZE);
#endif
  }

  /* initialize detector and ControlHost */
  my_daq_init (Det, args, argv);
  
  for(loop=0; ; loop++) {  // loop over commands
    char tg[TAGSIZE+1];  // tag from ControlHost (TAGSIZE from dispatch.h)
    
    if(G.Dbg>0) printf("Loop %d, awaiting next %s\n",loop,DAQCMD); // DAQCMD from daq.h

    int nbytes;
    rc=wait_head(tg, &nbytes);
    if(G.Dbg>1) printf("got tag=%s nbytes=%d\n",tg,nbytes);
    
    rc=get_string(cbuf,sizeof(cbuf)-1);
    assert(rc>=0);

    // parse the DAQCMD message
    cmd_args = cmd_flds(cbuf,&cmd_argv); // !! do not modify cbuf until after det_reactions!
    
    if(G.Dbg>0) {
      printf("%s Received %s, with %d args:",Det->name, tg,cmd_args);
	for(i=0; i<cmd_args; i++) {
	  printf("%d=|%s| ",i, cmd_argv[i]); 
	};
      printf("\n"); fflush(stdout);
    }
    if(cmd_args == 0) continue;  // ignore empty DAQCMD
    
    // is it a valid command?
    DaqCommand_t cmd = daq_cmd(cmd_argv[0]);
    if(cmd == WRONG_CMD) continue;

    // acknowledge recept of the command
    i=sprintf(line,"%s %04X <%s",cmd_argv[0], Det->partId,localHost());
    rc = put_fullstring("DAQACK",line);
    assert(rc>=0);
    
    // react to the command.
    rc=my_daq_reactions(Det, cmd,cmd_args,cmd_argv);

    // report on completion of the command execution
    i=sprintf(line,"%s %04X %d <%s",cmd_argv[0], Det->partId,rc,localHost());
    rc = put_fullstring("DAQDONE", line);
    assert(rc>=0);

  } // end of loop over commands
  return(0);
}



/*------------------------------------------------------------------------ my_daq_init */
int my_daq_init(Detector_t *det, int args, char**argv) {
  /* 
   * 1. get the parametrs required for DAQ and detector initialization 
   *    (hard-coded or from the command line)
   * 2. Create the detector object (Detector_t *Det)
   * 3. connect to to ControlHost
   */

  char *host= getenv("CONTROL_HOST");
  char line[200];   // short-lived char buffer for sprintf

  /* partition nickname template; re-define or specity it in command line */
  char DetName[20] = {"Mydet0_LocDaq_BAD0"}; //  "_LocDaq_" root is obligatory

  int rc; 
  uint16_t partId;

  if(args <= 1) {
    printf("Usage: %s partName  [CHost [AvNhits]]\n",basename(argv[0]));
    printf("  where partName is like Pixels2_LocDaq_0802 or DT0_LocDaq_0C00\n"
	   "  CHost is the Dispatcher hostname (default: $CONTROL_HOST)\n"
	   "  AvNhits is the average number of hits (default: 100)\n");
    exit(0);
  }

  // if not specified in command line, use $CONTROL_HOST as default 
  if(args>2) host=argv[2];
  if(host != NULL) strncpy(G.CHost,host,CHNAMELEN);  

  /* enable or disable recording (actually, it's disabled by default) */ 
  //G.options.recording = true;
  G.options.recording = false;

  // subscribe to ControlHost, listen only to DAQCMD command tags
  sprintf(line,"a %s",DAQCMD);  // DAQCMD="DAQCMD" in daq.h

  rc=init_2disp_link(G.CHost,"w dummy", line);
  if( rc < 0 ) {
    printf("Error: no connection to %s\n",host);
    exit(1); /* no connection */
  }
  /* check the nickname */
  strncpy(DetName,argv[1],sizeof(DetName)-1); // take cmdline arg
  char *c=strstr(DetName,"_LocDaq_"); 
  assert(c != NULL);   // obligatory _LocDaq_ root
  assert(*(c-1) == DetName[strlen(DetName)-1]); // partition number 
  c=strrchr(DetName,'_')+1; // partition ID
  partId = strtoul(c,NULL,16);
  assert(partId >=0x0800 && partId <=0x0C00); // official partId range

  /* create the detector object of type Detector_t (see main header) */
  Det = (Detector_t*) malloc(sizeof(Detector_t));

  /* copy detector-specific information and the spill buffer details to the detector object */
  assert(sizeof(DetName)<=sizeof(Det->name));
  strcpy(Det->name,DetName);
  Det->partId = partId;
  Det->buf = B;  // B is defined in the main header
  Det->sim.AvNhits = (args >3)? atoi(argv[3]) : 100; // only used for the simulation mode
  Det->sim.dly = (args >4)? atoi(argv[4]) : 100; // us, only used for the simulation mode

  printf("%s AvNhits=%d dly=%d us\n",Det->name, Det->sim.AvNhits, Det->sim.dly);

  Det->enabled = true;   // initially, the detector is enabled
 		 
  // report the nickname to ControlHost
  assert(my_id(DetName));

  // compose the tag for raw data (historically stored in G, not in Det...)
  assert(sizeof(G.RawTag)>8);
  sprintf(G.RawTag,"RAW_%04X",partId); // exactly 8 characters

  // ControlHost options
  send_me_always();

  return 0;
}


 


/*-------------------------------------------------------------------------- det_reactions */
int my_daq_reactions (Detector_t *det, 
		      DaqCommand_t cmd, int cmd_args, char **cmd_argv) {
  int i, rc=0, Nevts;
  DataFrame *buf;
#ifdef DT_READ
  static char *fn=NULL;       
#endif

  //printf(">> daq_reactions %d (%s)\n",cmd, DaqCmds[cmd]);

  switch (cmd) {

  case SOR : {  // --------------------------------------------------------- SoR 
    // Format: 
    // SoR [run num {prev+1}] [list of detectors {all enabled}]
    
    char detector[20];
    G.RunNumber = (cmd_args>0)? atoi(cmd_argv[1]) : G.RunNumber+1;
    
    // the detector "enabled" status remain unchanged, unless a list
    // of detectors is specified in the SoR command
    if(cmd_args>2) {
      det->enabled = false;
      for(i=2;i<cmd_args;i++) {
	if(strstr(det->name, cmd_argv[i]) != NULL) {
	  det->enabled=true;
	  strncpy(detector,cmd_argv[i],sizeof(detector)-1);;
	  break;
	}
      }
    } else {
      /* extract the detector name from the partition name */
      int c=strcspn(det->name,"012");
      strncpy(detector, det->name,c);
      detector[c+1]='\0';
    }
    if(!det->enabled) break;

    if(G.Dbg>0) printf("Start new run %d (%s/%s)\n", G.RunNumber,det->name,detector);

    /* reset detector at start of run */
    my_det_reset(det);     

#ifdef DT_READ
    dtdaq = popen("~/daq/testdaq/bin/dtdaq","w"); // launch dtdaq
    if(dtdaq == NULL) {
	fprintf(stderr,"Error: could not start dtdaq\n");
	my_daq_close(101);
    }
    setbuf(dtdaq,NULL);  // no buffering in the pipe
    fputc('a',dtdaq);  // start data taking

    {
      char *dataDir=getenv("RAWDATA_PATH");
      if(dataDir == NULL) {
	fprintf(stderr,"Error: RAWDATA_PATH is undefined\n");
	my_daq_close(102);
      }
	
      fn = dt_read(dataDir);
      if(fn == NULL) {
	fprintf(stderr,"Error: DT rawdata in RAWDATA_PATH=%s could not be opened\n",dataDir);
	my_daq_close(103);
      }
    }

#endif

    /* reset run statistics at start of run */
    for(i=0; i<NERRS; i++) det->errs[i]=0;      // error counters
    det->max_buffer = 0;       // largest spill buffer size;
    det->max_hits = 0;         // max no. of hits
    
    // store SoS special frame (with a dummy info) 
    buf=my_dataFrame(det,SOR_FLAG,"Run Config", 12 ); 

    buf->header.cycleIdentifier = G.RunNumber; // Run number -> CycleIdentifier
    
    printf("G.options.recording: %d\n",G.options.recording); fflush(stdout);
    // new run directory
    if(G.options.recording) {
      if ((G.RecDir=daq_mkdir(G.RunNumber, detector, &rc))==NULL) {
	printf("Error: output directory for run %d(%s) could not be created or opened.\n"
	       "rc=%d, recording is disabled\n", G.RunNumber,detector,rc);
	G.options.recording=false;
      }
    }
    // dump (or print) SoR frame
    my_det_dumpCycleData(det); 

    break;
  }
    
  case EOR : {   // ---------------------------------------------------------- EoR
    // Format:  EoR

#ifdef DT_READ
  if(dtdaq != NULL) {
    char cmd[200];
    printf("sending w/q to dtdaq\n");
    fputc('w',dtdaq);  // pause data taking
    fputc('q',dtdaq);  // stop DAQ and close output files
    pclose(dtdaq);
    dtdaq=NULL;
    *(strchr(fn,'.'))='\0';  // remove fn extension
    printf("Deleting %s.*\n",fn);
    sprintf(cmd,"rm %s",fn);
    system(cmd);
  }
#endif

    my_det_summary(det);
    if(G.Dbg>0) 
      printf("End of Run: %s\n", det->name);

    /* reset h/w etc */
    my_det_reset(det); 

    /* reset run statistics*/
    for(i=0; i<NERRS; i++) det->errs[i]=0;      // error counters
    det->max_buffer = 0;       // largest spill buffer size;
    det->max_hits = 0;         // max no. of hits
    
    // store SoS special frame (with a dummy info)
    buf=my_dataFrame(det,EOR_FLAG,"Run End Stats", 14 ); 
    buf->header.cycleIdentifier = G.RunNumber; // Run number -> CycleIdentifier
    
    // dump (or print) EoR frame
    my_det_dumpCycleData(det); 
    break;
  }

  case SOS : {   // --------------------------------------------------------- SoS
    // Format: 
    // SoS [hex spillID ]

    if(cmd_args >= 2) { // take new CycleId and, optionally, nev 
      sscanf(cmd_argv[1],"%x",&G.CycleID);
      if(G.Dbg>1) printf("SoS: Next CycleId=0x%X\n", G.CycleID); fflush(stdout);
    }

    G.inspill = true;
    
    /* reset detectors at start of spill      */
    my_det_reset(det);     
    
    /* store SoS frame */
    buf=my_dataFrame(det,SOS_FLAG, NULL, 0 ); // store EoS special frame

    waitSoS(); // wait for arrival of the SoS signal

    /* read spill event. Store the entire spill data in det->buf (see detector.h) */
    Nevts=0;
    
    do { // loop over triggers, while in spill
      uint32_t TrigTime;

      wait_trigger();

      TrigTime = latchTimestamp(); // in 40 MHz clock ticks
      
      if(G.Dbg>2) printf("   Gen event %d, Timestamp=%u)\n",Nevts,TrigTime);

      buf=my_det_getNextEv(det);  
      if(buf==NULL) break;        // cannot take further spill data (h/w EoS signal or buf ovfl)

      Nevts++;

      // fill frameTime and timeExtent fields in the frame header 
      // (overwrite the default values stored in det_detNextEv)
      buf->header.frameTime= TrigTime;
      buf->header.timeExtent=Nevts;
    } while (inspill()) ;

    // special EoS frame. Convention (MJ): Nevts+1 in the timeExten of the EoS frame
    buf=my_dataFrame(det,EOS_FLAG, NULL, 0 ); // store EoS special frame
    buf->header.timeExtent=Nevts+1; 
    // store last event error, in particular E-BUF-OVFL
    buf->header.flags = det->lasterr;  
    
    // done with spill data generation
    if(G.Dbg>0) printf("====== G.CycleID=%d: generated %6d events\n",
		     G.CycleID,Nevts); fflush(stdout);
    
    // dump (or print) spill buffer contents
    my_det_dumpCycleData(det); 
    rc = Nevts; // return the number of actually generated events
    break;
  } // end of "SoS" command

  case EOS : {   // --------------------------------------------------------- EoS 
    // Format: EoS 
    G.inspill = false;
    break;
  }
  case STOP :{  // ---------------------------------------------------------- Stop
    // Format: Stop [list of partitions, like Pixels, Pixels2, DT {all} ]

    if(cmd_args>1) {
      for(i=1;i<cmd_args;i++) 
	if(strstr(det->name, cmd_argv[i]) != NULL)  my_daq_close(0);    break;
    }
    my_daq_close(0) ;
    break;
  }
  case ENABLE :  { // ------------------------------------------------------- Enable

    // To enable detectors dynamically and change the detector parameters
    // using a "getopt" syntax, e.g.: 
    //    "Enable [-r {0}] [-d{1}] ..... list of detectors".
    // Accepts arbitary permutations of arguments, joint or separate values, e.g.
    // Enable -r 250 Pixels -d2   
    // (all Pixels partitions are enabled, with recording turned off and debug level set to 2 )
    // To change selected partitions, add partition number to the name, e.g. "Enable Pixels2" 

    for (i = 1; i < cmd_args; i++) 
      if(strstr(det->name,cmd_argv[i]) != NULL) {
	det->enabled=true;

	/*  options are detector-dependent; here only an example of parsing is shown */
	i=0;
	while(++i < cmd_args) {
	  if (cmd_argv[i][0] == '-') {
	    char *opt = cmd_argv[i]+1, c=*opt, *optarg= opt+1; // value joint with the tag
	    if(*optarg == '\0' && i+1< cmd_args) optarg = cmd_argv[i+1]; // blanc-separated value
 	    switch (c) {
	      case 'r':  G.options.recording = atoi(optarg);   break;
	      case 'd':  G.Dbg               = atoi(optarg);   break; 
	      default: fprintf(stderr,"invalid option %s\n",cmd_argv[i]); break;
	    }
	    assert(errno != ERANGE);
	  }
	}
	if(G.Dbg>0) 
	  printf("Enable %s, with effects: recording %d debug level=%d\n",
		 det->name, G.options.recording, G.Dbg);
	
	break;
      }
    break;
  }
  case DISABLE : { // --------------------------------------------------------- Disable
    for (i = 1; i < cmd_args; i++) {
      //printf(" %d Det.name %s ,cmd_argv[i] %s\n",i,det->name,cmd_argv[i]);
      if(strstr(det->name,cmd_argv[i]) != NULL) {	
	det->enabled=false;
	/* all options are disabled */
	G.options.recording=false;
	// ...

	if(G.Dbg>0) 
	  printf("Disable %s, reset all effects\n", det->name );
	break;
      }
    }
  }
  default:
    rc=-1;
  } // end of switch (cmd)
  return (rc);
}


/*-------------------------------------------------------------------------- signal_handler */
  /*
   * signal callback (used to intercept Ctrl-C and abort signals 
   */
void signal_handler(int signum) {
  printf("\n%s is closing on  %d signal \n",Self, signum);
  my_daq_close(signum);
}

/*-------------------------------------------------------------------------- my_daq_close */
void my_daq_close(int exitcode) {
  if(B.BufSizeAlloc != 0) free(B.spbuf);
#ifdef DT_READ    
  if(dtdaq != NULL) {
    fputc('w',dtdaq);  // pause data taking
    fputc('q',dtdaq);  // stop DAQ and close output files
    pclose(dtdaq);
  }
#endif
  exit(exitcode);
}

/*----------------------------------------------------------------------- spill simulation */
#define SPILL_LEN 0.5  //s
#define HW_DEADTIME 15
#define TRIGGERS_PER_SECOND 10000
#define SOS_TO_SPILL 10000 // us = 10 ms

const int trg_intrv_us=1000000/TRIGGERS_PER_SECOND;

static uint32_t us=0; // a dummy timer
bool inspill(void) {
  /*
   * In real detector, this function reflects the DAQ status set by SoS and EoS h/w signals
   * -- The demo code just simulates the timeline by random timer increments equivalent
   * -- to trigger occurences
   */
  const static uint32_t spill_len = SPILL_LEN*1000*1000;
  const static int trg_intrv_us=1000*1000/TRIGGERS_PER_SECOND;
  uint32_t d_us=0;

  while(G.inspill && (d_us += PoissnT(trg_intrv_us))<HW_DEADTIME); // simulate h/w dead time
  us += d_us;
  if(us >spill_len) {
    return false;
  }
  else return true;
}

void wait_trigger(void) {
  /*
   * In real detector, this function waits for the occurrence of a spill event 
   * which is equivalent to the arrival of the Trigger signal
   * -- The demo version: do nothing (the trigger time is simulated by inspill() )
   */
    
}

uint32_t latchTimestamp() {
  /*
   * In real detector, this function latches the local 40 KHz clock counter ("timestamp")
   * -- The demo code just returns the timer managed by the dummy inspill function
   */
  return us*40;
}

void my_det_resetClock(Detector_t *det) {
  /*
   * In real detector, this function resets the local 40 KHz clock counter ("timestamp")
   * -- The demo code resets the s/w timer
   */
  us=0;
}



void waitSoS (void) {
  /*
   * In real detector, this function awaits the SoS signal and resets the clock
   * -- The demo code simulates the arrival of SoS, resets the clock 
   *    and generates the random interval unil the 1st trigger
   */
  time_t tp;
  //  if( !inspill() ) { //
    srand(time(&tp)); // randomize
    my_det_resetClock(Det);   // clock reset
    while (us<SOS_TO_SPILL) { // 1st trigger delay after the SoS signal
      us += PoissnT(trg_intrv_us);
    }
}
