#include <string.h>
#include "daq.h"

/* 
 * Global variables, parameters and options (GlobPars_t is defined in  daq.h)
 * Initialized here with default values: 
 */
GlobPars_t G = {
#ifndef __cplusplus
  .RunNumber = -1, 
  .CycleID   = 0,
  .CHost     = {""},
  .RawTag    = {""}, 
  .RecDir    = "",
  .Dbg       = 1,
  .options   = {
      .chksum    = false, 
      .recording = false}
#else
  // c99 designated initialisers are not alowed in C++
 -1, 
  0,
  "",
  "", 
  (char *)"",
  1,
  { false, false}
#endif
}; // G: extern in daq.h




const char *DaqCmds[]={
  "SoR", "EoR", "SoS", "EoS",  // run control commands
  "Enable", "Disable", "Stop", // service & test commands
  NULL};

DaqCommand_t daq_cmd(char *command) {
  int i=0;
  while (DaqCmds[i] != NULL) {
    if(strcmp(command,DaqCmds[i]) == 0 ) return (DaqCommand_t)i;
    i++; }
  return WRONG_CMD;
}

//------------------------------------------------------------------------- mkdir package

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <errno.h>
#include <assert.h>

int do_mkdir(const char *path, mode_t mode, int direxists) {
 /*
  * makes a new dir. Returns:
  * = 0  : success
  * =-1 : system failed to make dir (except due to a collision with another mkdir)
  * = direxists : directory already exists 
  * Author:  pub. domain, modified by PG Apr 2018     */
 

  struct stat st; // <sts/stat.h>
  int             status = 0;

  // Directory does not exist. EEXIST for race condition 
  if (stat(path, &st) != 0) {
    if (mkdir(path, mode) != 0 && errno != EEXIST) status = -1;
  }
  // path exists and is....
  else {
    if (!S_ISDIR(st.st_mode)) {  // ... not a directory
      errno = ENOTDIR;
      status = -1;
    }
    // path exists and is a directory
    else status = direxists;   // ... directory
  }
  return(status);
}


int mkpath(const char *path, mode_t mode) {
  /*
   * creates "path", making missing directories, if needed 
   * return: 0  - new directory is created
   *         -1 - path exists ans is not a directory
   *         -2 - full path already exists
   * Author:  pub. domain, modified by PG Apr 2018     */

  char *pp,  *sp,  *copypath = 
                   strcpy((char *)malloc(strlen(path)+1),path);
  int             status=0;

  // first, check the path
  
  pp = copypath;
  while (status == 0 && (sp = strchr(pp, '/')) != 0)   {
    if (sp != pp) {
      *sp = '\0';
      status = do_mkdir(copypath, mode, 0);
      *sp = '/';
    }
    pp = sp + 1;
  }
  // finally, make the directory or return -2 if it already exists
  if (status == 0)  status = do_mkdir(path, mode,-2);
  free(copypath);
  return (status);
}

//------------------------------------------------------------------- daq_mkrundir $RAWDATA_PATH/
 char *daq_mkdir(int run, char *detector, int *rc) { 
  static char line[300]="";
  char *rawpath =  getenv("RAWDATA_PATH");
  uint16_t i,v=0;

  if(rawpath==NULL) {
    fprintf(stderr,"Undefined envvar $RAWDATA_PATH\n");
    *rc=-3;
  } else {

    i=sprintf(line,"%s/%s/Run.%05d",rawpath,detector,run);
    assert(i<sizeof(line));
    *rc = mkpath(line,0777);
    if(G.Dbg>1) {
      printf("Creating dir %s, rc=%d\n",line,*rc); fflush(stdout);}
    while (*rc == -2) {
      if(++v >5) break;
      sprintf(line+i,"(%d)",v);
      *rc = mkpath(line,0777);
      if(G.Dbg>1) {
	printf("Creating dir %s, rc=%d\n",line,*rc); fflush(stdout);}
    }
  }
  return (*rc<0)? NULL: line;
 }

int daq_chmod(const char *path, int mode) {
  return chmod(path,(mode_t)mode);
}


