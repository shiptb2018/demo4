#define _BSD_SOURCE  // needed if c99 compiler option is used

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <assert.h>

#include "daq.h"
#include "my_detector.h"
#include "buffer.h"
#include "daq_util.h"

/*---------------------------------------------------------------------------------------------- my_det_getNextEv    */
DataFrame *my_det_getNextEv(Detector_t *det) {
/* -- sample r/o routine
 * returns a pointer to the event data frame, or NULL in the case of buffer overflow
 * If "buffer overflow" occurred, a DataFrameHeader header with E_BUF_OVFL flag 
 * is written to the end of buffer and further  writing to the buffer is inhibited
 *
 */
  DataFrame *buf;


  // first r/o action: latch local timestamp
  uint16_t detFlags=0;  // subsystem-specific flags
  int hitsSize;
  uint32_t Nhits;  // expected number of hits

#ifndef DT_READ

  /*
   * simulated detector r/o: Nhits is known before the actual readout
   */
    Nhits = PoissnN(det->sim.AvNhits);               // random number of hits

  /*
   * In real r/o,

  Nhits= (... number of hits unknown... )? MAX_NHITS : actual #hits;

   * and call  my_updateFrameSize(det,sizeof(RawDataHit)*Nhits) , if real #hits > Nhits
   */

  /* allocate buffer for the event (return value NULL means insufficient memory) */
  hitsSize = sizeof(RawDataHit)*Nhits;
  buf=my_dataFrame(det, 0, NULL, hitsSize);
#else
  DataFrame *dt_buf = 
    (DataFrame *)dt_read(NULL);  // read DT rawdata from file
  hitsSize = dt_buf->header.size  - sizeof(DataFrameHeader);
  Nhits = hitsSize / sizeof(RawDataHit);
  //printf("after dt_read: size=%X\n",dt_buf->header.size ); fflush(stdout);
  if(dt_buf->header.size == 0xFFFF)  return NULL;   // eof reached, do nothing
  else {
     buf=my_dataFrame(det, 0, NULL, hitsSize);
     if(buf != NULL) memcpy(buf->hits, dt_buf->hits, hitsSize); 
     // preserve DT flags (in future, to be confined in the MSB!)
     detFlags = dt_buf->header.flags;  
  }
#endif
  
  if(buf != NULL) {  // successful event memory allocation 
    unsigned int bufsize=det->buf.nextfree - det->buf.spbuf;
    if(bufsize > det->max_buffer) det->max_buffer = bufsize; // update max buf size

  /* store all client-specific flags

  detFlags=...

  */

  /*
   * fill the header
   */
    buf->header.frameTime =0; // timestamp, to be filled in the calling routine
    buf->header.timeExtent=0; // sequential trig. number, to be filled in the calling routine
    buf->header.flags= detFlags;  // user-defined flags  (prefer to store then in the MSB)

   /*
    *  get and store hits data (meta code)
    *
      int i=0; 
      while(...any more hits? ...) { 
	    // get next hit
	    i++;
	    buf->hits[i].channelId=...;
	    buf->hits[i].hitTime=...;
	    #ifdef EXTRA_DATA
	    buf->hits[i].extraData=... 
	    #endif
	    if(i >= Nhits) break; // avoid buffer overflow
       }
       if(i < Nhits) {
           my_updateFrameNhits(det, nhits);
	   Nhits=i;
       }
     */
    if(Nhits > det->max_hits) det->max_hits = Nhits; // update hits statistics

  } else { 
    /*
     * handle the buffer overflow condition:
     * write the "buffer overlow" header, inhibit further writing to the spill buffer
     */
    my_det_err(det, E_BUF_OVFL);
    buf_inhibit(&(det->buf));
  }
  return buf;
}

/*-------------------------------------------------------------------- my_dataFrame */
DataFrame *my_dataFrame
     (Detector_t *det, uint32_t fType, const void *info, int nbytes ) {

/* Allocates a standard data frame of type "fType" (optionally with "nbytes" of data from "info") 
 * and fills the frame header, with .timeExtent=0 and .frameTime=fType.
 *
 * For regular events, use fType=0 and nbytes=sizeof(RawDataHit)*Nhits.
 * (if Nhits is a priori unknown, specify nbytes=MAX_EV_SIZE, see sample  my_det_getEvent 
 * above) 
 *

 * "Special" frame consist of a header with an optional (usually text) information 
 * block of "nbytes" bytes.
 *
 * Special frames are identified by the ftype=0xFF00xxxx copied to the timeFrame header field.
 * The standard special frame types are defined in daq.h as
 * SOR_FLAG, EOR_FLAG, SOS_FLAG and EOS_FLAG
 *
 * If *info!=NULL, then "nbytes" of "info" is copied to the data frame.
 *
 * Function returns a pointer to the data frame, or NULL in the case of buffer overflow
 */

  DataFrame *buf;
  int evsize = sizeof(DataFrameHeader)+nbytes; 

  /*get the buffer address for the event (NULL means insufficient memory) */
  buf=(DataFrame *)buf_getAddr(&(det->buf),evsize, det->name); 

  if(buf != NULL) {  // successful event memory allocation 

    //printf("%s ev %d Nhits=%d evsize=%d\n",det->name, det->buf.ev,Nhits,evsize); fflush(stdout);
    /*
     * fill the header
     */
    buf->header.size=evsize;
    buf->header.partitionID=det->partId;
    buf->header.cycleIdentifier=G.CycleID;  // if SoR, RunNumber is store here
    buf->header.timeExtent=0;  // to be modified in the calling routine, if needed
    buf->header.flags=0;
    // 
    buf->header.frameTime= fType; 
    buf->header.flags = 0xFACE;  // temporary, to facilitate hexdump reading

    /*
     * fill the data part, if "info" != NULL
     */
    if(nbytes>0 && info !=NULL)  memcpy((char *)buf->hits, info,nbytes);
  }
  return buf;
}




/*---------------------------------------------------------------------------------------------- det_err */
void my_det_err(Detector_t *det, int err) {
  det->errs[err-1] += 1;    // increment error counter
  det->lasterr = err;
}

/*---------------------------------------------------------------------------------------------- det_reset */
int my_det_reset(Detector_t *det) {
  /* reset detector at start of spill */

  buf_reset(&(det->buf),det->name);   // reset the spill buffer  
  det->lasterr=0; // reset last subsystem error
  
  /* other system-related resets
  ...
  */

  return 0;
}


/*-------------------------------------------------------------------------- det_dumpCycleData */
void my_det_dumpCycleData(Detector_t *det) {
  int ev=0;
  
  char *adr=det->buf.spbuf;             // pointer to the spill buffer
  DataFrame *buf = (DataFrame *)adr;
  uint32_t blockType = buf->header.frameTime; 
  uint32_t total_sz=0;
  FILE *outfile=NULL;

  if(G.Dbg>0) printf("--- dumping with tag |%s|\n",G.RawTag);
 
  // loop over frames, publish each frame separately to ControlHost
  while(buf->header.size != 0) {
    ev++;
    total_sz += buf->header.size;

    if(G.Dbg>2) {    // debug print
      printf("ev %4d size %5d cycleID=%x frameTime %u(0x%08X) timeExtent %u\n",
	     ev,
	     buf->header.size,
	     buf->header.cycleIdentifier,
	     buf->header.frameTime, buf->header.frameTime, 
	     buf->header.timeExtent); 
      fflush(stdout);
    }

    put_fulldata(G.RawTag,adr,buf->header.size);   // use put_fulldata!!
    usleep(random() % det->sim.dly);
    //    adrlast = adr; // save last event address
    adr += buf->header.size;
    buf = (DataFrame *) adr;
  }

  if(total_sz > det->max_buffer) det->max_buffer = total_sz; // update max buf size

  //printf("Buf summary for detector %s: %d events, spill size %d bytes\n",
  //	   det->nickname,ev,total_sz);

  /*  dump spill data to a standardized local filenames in G.DecDir directory */
  if(G.options.recording) {
    char line[300];
    // define the filename from the 1st frame type (SoS, SoR, EoR or anything else)
    //    buf = (DataFrame *)det->buf.spbuf;
    switch (blockType) {
    case SOS_FLAG : 
      sprintf(line,"%s/SPILLDATA_%04X_%u",G.RecDir,det->partId,G.CycleID); break;
    case SOR_FLAG : 
      sprintf(line,"%s/RUNSTART_%04X_%u",G.RecDir,det->partId,G.RunNumber); break;
    case EOR_FLAG : 
      sprintf(line,"%s/RUNEND_%04X_%u",G.RecDir,det->partId,G.RunNumber); break;
    default :
      sprintf(line,"%s/SOMEDATA_%04X_%u",G.RecDir,det->partId,G.CycleID);
    }
    if((outfile = fopen(line,"w"))==NULL) {
      fprintf(stderr,"Error opening file %s for writing. Recording is disabled\n",line);
      G.options.recording=false;
    } else {
      fwrite(det->buf.spbuf, 1, total_sz,outfile);
      fclose(outfile);
      daq_chmod(line,0444); // protect against deleting data by accident
    }
  }
}

/*---------------------------------------------------------------------------------------------- det_summary */
void my_det_summary(Detector_t *det) {
  const char *FlagMsg[NERRS] = {       // error flags meaning. NERRS from daq.h
    "Undefined error",
    "Spills truncated: buffer overflow"
};

  int i;
  const char *dynstat = (det->buf.BufSizeAlloc==0)? "(stat)" : "(dyn)";
  printf("-------Detector %s:\n", det->name);
  for(i=0; i<NERRS; i++) 
    if(det->errs[i]>0) printf(" >> %s (err %2d): %d\n",  FlagMsg[i],i+1, det->errs[i]);

  printf("  last error: %d\n",det->lasterr);
  printf("  longest spill buffer: %5.1f MB, allocated %s: %5.1f MB\n",
	 det->max_buffer*1.E-6, dynstat,det->buf.spbuf_size*1.E-6);
  printf("  max no. of hits: %d\n",det->max_hits);
  printf("\n");
}

