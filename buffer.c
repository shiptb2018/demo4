#define _BSD_SOURCE  // needed if c99 compiler option is used

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>

#include "buffer.h"

void *buf_getAddr(Buffer_t *buf, int evsize, char *name) {
/* -- sample dynamic buffer manager
 * *buf - pointer to the Buffer_t structure holding the buffer variables
 * *name - a name to identify this buffer in debug prints (not used for memory operations)
 * evsize - event (data frame) size, should be >0
 */

  // to make code more transparent, copy structure members to local variables
  char *spbuf       = buf->spbuf;
  char *nextfree    = buf->nextfree;
  size_t spbuf_size = buf->spbuf_size;
  size_t bufSzAlloc = buf->BufSizeAlloc;

  char *adr;
  static const int margin=30;  // ~ 16 + 10  margin for the end-of-buffer marker and Frame Header

  assert (evsize>0 && buf !=NULL);
  // printf("==getEvBufAddr esize=%d, detname=%s nextfree=%p\n",evsize,name,nextfree); fflush(stdout);

  // initial buffer allocation
  if(spbuf==NULL) {
    spbuf = (char *)malloc(bufSzAlloc); 
    spbuf_size = bufSzAlloc;
    memset(spbuf,0,spbuf_size);
    nextfree=spbuf;
    printf (">>> new spill buffer is allocated for \"%s\",  size=%ld, p(spbuf) %p\n",
	    name,spbuf_size, spbuf);
    //    printf(" sizeof(DataFrameHeader) %ld sizeof(RawDataHit) %ld\n", 
    //    sizeof(DataFrameHeader),sizeof(RawDataHit)); fflush(stdout);
  } 

    if(bufSzAlloc>0) {              // dynamic buffer
    // reserve space for the next event. Reallocate dynamic buffer, if needed
      while(nextfree-spbuf + evsize + margin > (int) spbuf_size) {
	int offset=nextfree-spbuf;
	spbuf=(char *)realloc(spbuf,spbuf_size+bufSzAlloc);
	assert (spbuf != NULL);   // to be replaced with a real recovery code
	nextfree = spbuf+offset;
	spbuf_size += bufSzAlloc;
	memset(spbuf,0,spbuf_size);
	printf (">>> spill buffer is re-allocated for \"%s\",  new size=%6.1f MB",
		name,spbuf_size/1000000.); 
	fflush(stdout);
	printf (" -- p(spbuf): %p  p(nextfree)=%p\n", spbuf,nextfree);
      } 
    } else       {
      /*
       * check buffer overflow condition for statuc buffers
       */
      if(nextfree != NULL) {
	if (nextfree-spbuf + evsize + margin > (int)spbuf_size) {
	  //printf("===Det %s unsifficient buffer for with size %d, current filled %ld buf size %ld\n",
	  //name,evsize,nextfree-spbuf,(long int)spbuf_size); fflush(stdout);
	  memset(nextfree,0,spbuf_size-(size_t)(nextfree-spbuf));   // set the rest of the buffer to zeroes
	  return NULL;  // static buffer is too short
	}
      } else return NULL;  // buffer is "inhibited"
    }

    adr=nextfree;
    nextfree += evsize;
    buf->ev += 1;                   // increment event counter
    *((uint16_t *)nextfree)=0;      // end of buffer marker

  // save buffer variables back to the struct 
  buf->spbuf=spbuf;
  buf->nextfree=nextfree;
  buf->spbuf_size=spbuf_size;

  return (void *)adr;
}


int buf_reset(Buffer_t *buf, char *name) {
  assert(buf != NULL);
  buf->nextfree=buf->spbuf;  // set the nextfree pointer to the buffer head
  buf->ev = 0;
  return 0;
}

void buf_inhibit(Buffer_t *buf) {
  /*
   * inhibits buffer allocation until next buf_reset
   */  
  buf->nextfree = NULL;
}
